var gulp = require('gulp');
var elm = require('gulp-elm');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');

var exercise = "exercise2";

gulp.task('watch-elm', function() {
  compileElm();
  return watch('src/' + exercise + '/*.elm', function () {
    compileElm();
  })
});

var compileElm = function () {
  return gulp.src('src/' + exercise + '/*.elm')
    .pipe(plumber())
    .pipe(elm.bundle('bundle.js'))
    .pipe(gulp.dest('dist/'));
}
