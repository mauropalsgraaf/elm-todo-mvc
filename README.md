# TodoMVC implementation in elm-lang

This repository can be used to practice elm and get used to the elm architecture. It contains an initial setup to work with elm-lang with ease. Also, it contains an almost full implemented todoMVC which is written in elm 0.18. It can be used to help you out whenever you get stuck. Also, it contains almost everything except 3 concepts: ports, nested components and Side effects. Later on, examples might be added containing implementations for these concepts.

## Prerequisites

Make sure elm v0.18 is installed.

In order to make use of this repository, make sure docker is install. It will pull an nginx container where the elm application will run in.

Also, gulp needs to be installed globally to have a watcher running that compiles your elm code. Gulp can be installed with the following command: `npm install -g gulp`

## How to use the project

Run the following command in the root of this project: `docker-compose up -d`. This will start the nginx container which serves the static file. The server html can be found at localhost:8080/index.html.

You can remove all the code in Main.elm which contains the (almost) full implementation of the todoMVC. You can run the watcher by using the following command in the terminal: `gulp watch-elm`.

## Examples

More examples of todomvc can be found at http://todomvc.com.
