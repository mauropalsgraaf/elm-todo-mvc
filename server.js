var express = require('express')
var bodyParser = require('body-parser')

var app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

var nextId = 4;

app.listen(3000, function () {
    console.log('Express app started at port 3000!');
});

app.get('/', function (req, res) {
    res.sendFile('/Users/mauropalsgraaf/Documents/Projects/LTP/BrownBag/index.html');
});

app.get('/all', function (req, res) {
    res.send(todos);
});

app.post('/add', function (req, res) {
    todos.push({id: nextId, title: req.body.title})
    res.send({id: nextId, title: req.body.title});
    nextId++;
});

app.put('/edit', function (req, res) {
    todos.map(
      function (todo) {
        if (todo.id == req.body.id) {
          todo.title = req.body.title;
        }

        return todo;
      }
    );

    res.send({id: req.body.id, title: req.body.title})
});

var todos = [
    {
	id: 1,
	title: 'Going Grocery shopping'
    },
    {
	id: 2,
	title: 'Doing Homework'
    },
    {
	id: 3,
	title: 'Going to the gym'
    }
];
