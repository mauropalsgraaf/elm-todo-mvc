module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Json.Decode exposing (..)


main =
    Html.program { init = init, update = update, view = view, subscriptions = (\_ -> Sub.none) }


type alias Model =
    { todos : List Todo
    , inputField : String
    , newId : Int
    , filterStatus : Maybe Status
    }


type alias Todo =
    { id : Int, title : String, status : Status }


type Msg
    = AddTodo
    | UpdateInputField String
    | ToggleStatus Int Status
    | RemoveTodo Int
    | UpdateFilterStatus (Maybe Status)
    | MarkAllAsCompleted
    | MarkAllAsActive
    | ClearCompleted


type Status
    = Active
    | Completed


init : ( Model, Cmd Msg )
init =
    ( { todos = [], inputField = "", newId = 0, filterStatus = Nothing }, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddTodo ->
            case model.inputField of
                "" ->
                    ( model, Cmd.none )

                otherwise ->
                    let
                        newTodo =
                            { id = model.newId, title = model.inputField, status = Active }

                        newTodos =
                            newTodo :: model.todos

                        newId =
                            model.newId + 1

                        inputField =
                            ""
                    in
                        ( { model | todos = newTodos, newId = newId, inputField = inputField }, Cmd.none )

        UpdateInputField newTitle ->
            ( { model | inputField = newTitle }, Cmd.none )

        ToggleStatus id currentStatus ->
            let
                newStatus =
                    case currentStatus of
                        Active ->
                            Completed

                        Completed ->
                            Active

                updatedTodos =
                    List.map
                        (\todo ->
                            if todo.id == id then
                                { todo | status = newStatus }
                            else
                                todo
                        )
                        model.todos
            in
                ( { model | todos = updatedTodos }, Cmd.none )

        RemoveTodo id ->
            let
                updatedTodos =
                    List.filter (\todo -> todo.id /= id) model.todos
            in
                ( { model | todos = updatedTodos }, Cmd.none )

        UpdateFilterStatus newStatus ->
            ( { model | filterStatus = newStatus }, Cmd.none )

        MarkAllAsCompleted ->
            let
                updatedTodos =
                    List.map (\todo -> { todo | status = Completed }) model.todos
            in
                ( { model | todos = updatedTodos }, Cmd.none )

        MarkAllAsActive ->
            let
                updatedTodos =
                    List.map (\todo -> { todo | status = Active }) model.todos
            in
                ( { model | todos = updatedTodos }, Cmd.none )

        ClearCompleted ->
            let
                updatedTodos =
                    List.filter (\todo -> todo.status /= Completed) model.todos
            in
                ( { model | todos = updatedTodos }, Cmd.none )


view : Model -> Html Msg
view model =
    div [ class "todoapp" ]
        [ header_ model
        , todosList model
        , footer_ model
        ]


header_ : Model -> Html Msg
header_ model =
    let
        addTodoInputField =
            input
                [ placeholder "What needs to be done"
                , class "new-todo"
                , onInput UpdateInputField
                , Html.Attributes.value model.inputField
                , onEnter AddTodo
                ]
                []
    in
        header []
            [ h1 [] [ text "todos" ]
            , addTodoInputField
            ]


todosList : Model -> Html Msg
todosList model =
    let
        markAllMsg =
            if List.any (\todo -> todo.status == Active) model.todos then
                MarkAllAsCompleted
            else
                MarkAllAsActive

        filteredTodos =
            filterTodos model.filterStatus model.todos

        todosToRender =
            List.map todoView filteredTodos
    in
        section [ class "main" ]
            [ input [ type_ "checkbox", class "toggle-all" ] []
            , label [ onClick markAllMsg ] [ text "Mark all as complete" ]
            , ul [ class "todo-list" ] todosToRender
            ]


footer_ : Model -> Html Msg
footer_ model =
    let
        nrOfActiveTodos =
            List.foldr
                (\todo count ->
                    if todo.status == Active then
                        count + 1
                    else
                        count
                )
                0
                model.todos
    in
        footer [ class "footer" ]
            [ div []
                [ span [ class "todo-count" ] [ text ((toString nrOfActiveTodos) ++ " items left") ]
                , ul [ class "filters" ]
                    [ li [ class ("all") ] [ a [ class (selected model Nothing), onClick (UpdateFilterStatus Nothing) ] [ text "All" ] ]
                    , li [ class "active" ] [ a [ class (selected model (Just Active)), onClick (UpdateFilterStatus (Just Active)) ] [ text "Active" ] ]
                    , li [ class "completed" ] [ a [ class (selected model (Just Completed)), onClick (UpdateFilterStatus (Just Completed)) ] [ text "Completed" ] ]
                    ]
                , button [ class "clear-completed", onClick ClearCompleted ] [ text "Clear completed" ]
                ]
            ]


todoView : Todo -> Html Msg
todoView todo =
    let
        todoTextClass =
            todoStyles todo
    in
        li [ class todoTextClass ]
            [ div [ class "view" ] []
            , input [ type_ "checkbox", class "toggle", onClick (ToggleStatus todo.id todo.status), checked (todo.status == Completed) ] []
            , label [] [ text todo.title ]
            , button [ class "destroy", onClick (RemoveTodo todo.id) ] []
            ]


selected : Model -> Maybe Status -> String
selected model filterStatus =
    if model.filterStatus == filterStatus then
        "selected"
    else
        ""


onEnter : Msg -> Attribute Msg
onEnter msg =
    let
        isEnter code =
            if code == 13 then
                succeed msg
            else
                fail "not ENTER"
    in
        Html.Events.on "keydown" (andThen isEnter Html.Events.keyCode)


filterTodos : Maybe Status -> List Todo -> List Todo
filterTodos filterStatus todos =
    case filterStatus of
        Nothing ->
            todos

        Just a ->
            List.filter (\todo -> todo.status == a) todos


todoStyles : Todo -> String
todoStyles todo =
    case todo.status of
        Active ->
            ""

        Completed ->
            "completed"
